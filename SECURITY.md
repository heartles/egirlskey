# Reporting Security Issues

If you discover a security issue in eGirlskey, please report it by sending an
email to [admin@heartles.xyz](mailto:admin@heartles.xyz).

This will allow us to assess the risk, and make a fix available before we add a
bug report to the GitDab repository.

Thanks for helping make eGirlskey safe for everyone.
