/*
 * SPDX-FileCopyrightText: syuilo and misskey-project
 * SPDX-License-Identifier: AGPL-3.0-only
 */

export function isUserMentioned(note: any, userIds: Set<string>): boolean {
	if (!note) {
		return false;
	}

	if (note.mentions != null && note.mentions.some(mention => userIds.has(mention))) {
    	return true;
	}

	if (note.renote != null && note.renote.mentions != null && note.renote.mentions.some(mention => userIds.has(mention))) {
    	return true;
	}

	return false;
}
