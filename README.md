# eGirlskey
---

An allowlist-federating shitposting engine.

## ✨ Features
- **Allowlist federation**\
Control server access! Reduce reply guys! Become the online hermit you always wanted to be!
- **Private API support**\
Lock down public pages and API access to signed-in users
- **ActivityPub support**\
Not on eGirlskey? No problem! Not only can eGirlskey instances talk to each other, but you can make friends with people on other networks like Mastodon and Pixelfed!
- **Federated Backgrounds and Music status**\
You can add a background to your profile as well as a music status via ListenBrainz, show everyone what music you are currently listening too
- **Mastodon API**\
eGirlskey implements the Mastodon API unlike normal Misskey
- **Sign-Up Approval**\
With eGirlskey, you can enable sign-ups, subject to manual moderator approval and mandatory user-provided reasons for joining.
- **Rich Web UI**\
       eGirlskey has a rich and easy to use Web UI!
       It is highly customizable, from changing the layout and adding widgets to making custom themes.
       Furthermore, plugins can be created using AiScript, an original programming language.
- And much more...

</div>

<div style="clear: both;"></div>

## Documentation

lol oops tba. ping @heartles@egirls.gay for questions or look at upstream Sharkey or Misskey documentation
